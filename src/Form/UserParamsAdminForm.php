<?php

namespace Drupal\shib_auth_ng\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class UserParamsAdminForm.
 *
 * @package Drupal\shib_auth_ng\Form
 */
class UserParamsAdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'shib_auth_ng.UserParamsAdmin',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_params_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('shib_auth_ng.UserParamsAdmin');
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('shib_auth_ng.UserParamsAdmin')
      ->save();
  }

}
